<?php 
namespace Tannhutha\LaravelApiHelpers\Helper;

class ApiResultPagination{
    
    private $changeDataKeyTo = 'data';

    private function __construct($dataKey){
        $this->changeDataKeyTo = $dataKey;
    }

    public static function withDataKeyAs(String $newDataKey){
        return new self($newDataKey);
    }

    public function paginate($model, $perPage = 25){
        $results = $model->paginate($perPage);
        if($results->currentPage() > $results->lastPage()){
            throw new \Illuminate\Database\Eloquent\ModelNotFoundException("Exceeded maximum number of pages. [details]: The page number '{$results->currentPage()}' does not exist.", 404);
        }
        $list = [
            'total' => $results->total(),
            'per_page' => $results->perPage(),
            'current_page' => $results->currentPage(),
            'last_page' => $results->lastPage(),
            'first_page_url' => $results->url(1),
            'last_page_url' => $results->url($results->lastPage()),
            'next_page_url' => $results->nextPageUrl(),
            'prev_page_url' => $results->previousPageUrl(),
            'path' => $results->getOptions()['path'],
            'from' => ($results->onFirstPage() ? 1 : (($results->currentPage() - 1) * $results->perPage()) + 1),
            'to' => ($results->count() >= $results->perPage() ? ($results->perPage() * $results->currentPage()) : (($results->perPage() * ($results->currentPage() - 1)) + $results->count())),
            'has_more' => $results->hasMorePages(),
            'data' => $results->all()
        ];
        if($this->changeDataKeyTo !== 'data'){
            $list[$this->changeDataKeyTo] = $list['data'];
            unset($list['data']);
        }
        return $list;
    }
}