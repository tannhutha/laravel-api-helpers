<?php 
namespace Tannhutha\LaravelApiHelpers\Helper;

use Konsulting\Laravel\Transformer\Transformer;
use Konsulting\Laravel\Transformer\RulePacks\CoreRulePack;
use Konsulting\Laravel\Transformer\RulePacks\CarbonRulePack;

class ApiRequestTransformer{

    public static function transform(Array $inputs, Array $transformerRules){
        $transformer = new Transformer([CoreRulePack::class, CarbonRulePack::class]);
        return $transformer->transform($inputs, $transformerRules);
    }
}