<?php
namespace Tannhutha\LaravelApiHelpers\Helper;

use Illuminate\Http\Request;

class ApiRequestValidator {

    public static function validate(Request $request, Array $validationRules) {
        $validator = \Validator::make($request->all(), self::parseRulesForPlaceholders($request, $validationRules));
        if ($validator->fails()) {
            return $validator;
        }

        return false;
    }

    public static function parseRulesForPlaceholders(Request $request, $validationRules): Array
    {
        $rules = [];
        foreach($validationRules as $column => $ruleString){
            if(!isset($rules[$column])){
                $rules[$column] = [];
            }
            if(\is_array($ruleString)){
                $rules = $ruleString;
                continue;
            }
            if(\is_string($ruleString)){
                $ruleStringParts = \explode('|', $ruleString);
                foreach($ruleStringParts as $aRule){
                    if(\preg_match('/^unique:(.*)/', $aRule)){
                        if(\preg_match('/{{(?:(\w+).?)(\w+)?}}/', $aRule, $matches)){
                            if(\sizeof($matches) === 2){
                                $requestInputKey = $matches[1];
                                $aRule = \str_replace($matches[0], $request->$requestInputKey, $aRule);
                            }elseif(\sizeof($matches) === 3){
                                $requestInputMethod = $matches[1];
                                $requestInputKey = $matches[2];
                                $requestInputValue = '';
                                if(\method_exists($request, $requestInputMethod)){
                                    $requestInputValue = \call_user_func_array(array($request, $requestInputMethod), [$requestInputKey]);
                                }
                                $aRule = \str_replace($matches[0], $requestInputValue, $aRule);
                            }
                        }
                    }
                    \array_push($rules[$column], $aRule);
                }
            }
            
        }

        return $rules;
    }
}