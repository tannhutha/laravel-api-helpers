<?php
namespace Tannhutha\LaravelApiHelpers\Helper;

use Storage;
use DateInterval;
use DateTime;

class ApiAuth0Token{

    const AUTH0_MANAGEMENT_API_PATH = '/api/v2/';

    private $httpClient, $httpRequest, $data;

    private $persistToken = false;

    private $options = [
        'returntransfer' => true,
        'encoding' => '',
        'maxredirs' => 10,
        'timeout' => 30,
        'http_version' => CURL_HTTP_VERSION_1_1,
        'json' => [
            'client_id' => 'YOUR_AUTHORIZED_APP_CLIENT_ID',
            'client_secret' => 'YOUR_AUTHORIZED_APP_CLIENT_SECRET',
            'audience' => 'https://your-tenant.auth0.com/api/v2/',
            'grant_type' => 'client_credentials'
        ]
    ];

    private function __construct(string $clientId, string $clientSecret, string $path){
        $this->httpClient = new \GuzzleHttp\Client(['base_uri' => 'https://' . \getenv('AUTH0_DOMAIN')]);
        $this->httpRequest = new \GuzzleHttp\Psr7\Request('POST', $path, [
            'Content-Type' => 'application/json'
        ]);
        $this->options['json']['client_id'] = $clientId;
        $this->options['json']['client_secret'] = $clientSecret;
    }

    public function __toString(){
        return $this->token();
    }

    public function token(){
        return $this->data->access_token;
    }

    public function expiresIn(){
        return $this->data->expires_in;
    }

    public function persist(){
        $this->persistToken = true;

        return $this;
    }

    public static function requestAccess(string $clientId, string $clientSecret, string $path = '/oauth/token'){
        return new self($clientId, $clientSecret, $path);
    }

    public function forManagementApi(){
        return $this->forAudience('https://' . \getenv('AUTH0_DOMAIN') . self::AUTH0_MANAGEMENT_API_PATH);
    }

    public function forAudience(string $auth0Audience){
        $this->options['json']['audience'] = $auth0Audience;
        if($this->persistToken === true && $this->readTokenFile($this->tokenFile($this->options['json']))){
            return $this;
        }
        $response = $this->httpClient->send($this->httpRequest, $this->options);
        $response = (string) $response->getBody();
        $this->data = @\json_decode($response);
        // Save token to a file
        if($this->persistToken === true){
            $this->writeTokenFile($this->tokenFile($this->options['json']), $response);
        }

        return $this;
    }

    private function tokenFile(array $params = array()){
        if(empty($params)){
            $params = $this->options['json'];
        }
        \ksort($params);
        $params = join('', $params);

        return \base64_encode(\sha1($params)) . '.apiauth0token';
    }

    private function writeTokenFile(string $file, string $content){
        $content = @\json_decode($content, true);
        // Decode and add our expires_on and expires_on_formatted stamps
        $expiresOn = new DateTime();
        $expiresOn->add(new DateInterval("PT{$this->expiresIn()}S"));
        $content['expires_on'] = $expiresOn->format('U');
        $content['expires_on_formatted'] = $expiresOn->format('Y-m-d H:i:s');
        // re-encode and write the json to a file
        $content = \json_encode($content);
        // Write to local storage
        return Storage::disk('local')->put($file, $content);
    }

    private function readTokenFile(string $file){
        $content = '{}';
        if(Storage::disk('local')->exists($file)){
            $content = Storage::disk('local')->get($file);
            $content = @\json_decode($content);
            if($content->expires_on > (time() - 10)){
                return $this->data = $content;
            }
        }
        return false;
    }
}