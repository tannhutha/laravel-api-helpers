<?php 
namespace Tannhutha\LaravelApiHelpers\Helper;

use Exception;
use Tannhutha\LaravelApiHelpers\Helper\ApiResponseCodes;

class ApiExceptionErrorAsJson{

    public static function render(Exception $exception){
        // Use the exception class as the error type
        $type = new \ReflectionClass($exception);
        $type = $type->getShortName();
        // Default to the http status code 500 
        $httpStatusCode = (($exception->getCode() > 0 && $exception->getCode() < 500) ? $exception->getCode() : 500);
        // Default to internal server error message & details
        $details = $exception->getMessage();
        $message = 'We experienced an internal server error.';
        $fields = [
            'file' => $exception->getFile(),
            'line' => $exception->getLine(),
        ];
        // Here we grap the actual message thrown the the exception and override the 500 internal server error
        if($httpStatusCode > 0 && $httpStatusCode < 500){
            $message = $exception->getMessage();
            $details = $message;
        }
        // Handle validation exceptions a bit differently by forcing a http code of 400 (Bad Request)
        if($type == 'ValidationException'){
            $fields = [];
            $httpStatusCode = 400;
            $message = 'One or more invalid request parameters.';
            $details = '';
            // Lets flatten the validation exception errors
            foreach($exception->errors() as $f => $m){
                $m = join('; ', $m);
                $fields[$f] = $m;
                $details = join(' ', [$details, $m]);
            }
        }
        // Handle query exceptions a bit differently by forcing a http code of 500 (Internal error)
        // Undefined column: 7 ERROR: column "fake"
        if ($type == 'QueryException') {
            $fields = [];
            $httpStatusCode = 500;
            $message = "We experienced a SQL error.";
            $details = "SQLSTATE[{$exception->getCode()}]";
            $rawMessage = $exception->getMessage();
            if(\preg_match('/Undefined column:.*column "(.*?)"/', $rawMessage, $matches)){
                $fields[$matches[1]] = "Undefined column \"{$matches[1]}\".";
            }
            // SQLSTATE[23505]: Unique violation ... DETAIL: Key (code)=(TNNH) already exists... 
            if (\preg_match('/Unique violation:.*\s+?DETAIL:.*\((.*?)\)(?:\s+)?=(?:\s+)?\((.*?)\)(?:\s+)?already exists/', $rawMessage, $matches)) {
                // $fields[$matches[1]] = "The column \"{$matches[1]}\" with value \"{$matches[2]}\" already exist.";
                $fields[$matches[1]] = "The value \"{$matches[2]}\" already exist.";
            }
        }
        // Parse for the message template "{message} [details]: {details}" and assign the {message}
        // portion as the error message and everything after [details]: as the details
        if (\preg_match('/\[details\]:\s+?(.*)$/', $message, $matches)) {
            $message = \str_replace($matches[0], '', $message);
            $details = $matches[1];
        }
        // Put everything together in a standardized error array
        $error = [
            'error' => [
                'type' => $type,
                'code' => ApiResponseCodes::codeToStandardizedStatus($httpStatusCode),
                'message' => $message,
                'details' => $details,
                'fields' => $fields,
            ],
        ];

        // Return it as json
        return response()->json($error, $httpStatusCode);
    }
}