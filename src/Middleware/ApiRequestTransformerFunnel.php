<?php
namespace Tannhutha\LaravelApiHelpers\Middleware;

use Closure;
use Route;
use Tannhutha\LaravelApiHelpers\ApiHelperFunctions;
use Tannhutha\LaravelApiHelpers\Helper\ApiRequestTransformer;

class ApiRequestTransformerFunnel
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $currentController = Route::current()->controller;
        if($currentController){
            $model = ApiHelperFunctions::getControllerModel($currentController);
            $request->merge(ApiRequestTransformer::transform($request->post(), $model::$transformerRules)->all());
        }
        return $next($request);
    }
}
