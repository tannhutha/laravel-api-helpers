<?php
namespace Tannhutha\LaravelApiHelpers\Middleware;

use Closure;
use Route;
use Illuminate\Validation\ValidationException;
use Tannhutha\LaravelApiHelpers\Helper\ApiRequestValidator;
use Tannhutha\LaravelApiHelpers\ApiHelperFunctions;

class ApiRequestValidationFunnel
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $currentController = Route::current()->controller;
        if($currentController){
            $model = ApiHelperFunctions::getControllerModel($currentController);
            if(is_array($model::$validationRules) && isset($model::$validationRules[$request->getMethod()])){
                $validator = ApiRequestValidator::validate($request, $model::$validationRules[$request->getMethod()]);
                if($validator){
                    throw new ValidationException($validator);
                }
            }
        }
        
        return $next($request);
    }
}
