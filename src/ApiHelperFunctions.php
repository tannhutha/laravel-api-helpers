<?php 
namespace Tannhutha\LaravelApiHelpers;

class ApiHelperFunctions{

    public static function getControllerModel(\App\Http\Controllers\Controller $controller): String 
    {
        $modelClass = '';
        if(\property_exists($controller, 'model')){
            if(!empty($controller::$model)){
                $modelClass = $controller::$model;
            }
        }else{
            $rootLaravelControllerNamespace = 'App\Http\Controllers';
            $controllerClass = \get_class($controller);
            $modelClass .= \str_replace($rootLaravelControllerNamespace, 'App', $controllerClass);
            // $modelClass = \rtrim($modelClass, 'Controller');
            $modelClass = \preg_replace('/Controller$/', '', $modelClass);
        }

        return $modelClass;
    }
}