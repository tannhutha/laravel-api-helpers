# Laravel Api Helpers

Middleware and Helper classes to aid in building a Laravel API.

## Prerequisites / Notes
- Requires Laravel request transformer package: **konsulting/laravel-transformer** (https://github.com/konsulting/laravel-transformer)

## Installation
### Via Composer
1) Add the gitlab repository to your composer.json
```
composer config repositories.laravel-api-helpers vcs https://gitlab.com/tannhutha/laravel-api-helpers.git
```
2) Composer require
```
composer require tannhutha/laravel-api-helpers:"^2.0.0"
```